let form = document.forms.pwd;
let passwordInput = form.elements.password;
let confirmPasswordInput = form.elements.confirmPassword;
let showPasswordIcon = document.querySelector('.icon-password');
let hidePasswordIcon = document.querySelector('.icon-password-hidden');
let showPasswordIconConfirm = document.querySelector('.icon-password-confirm');
let hidePasswordIconConfirm = document.querySelector('.icon-password-hidden-conform');
let text = document.querySelector('.show-error-text');
text.style.color = 'red';
text.style.marginTop = '-45px';


let showPassword = showPasswordIcon.addEventListener('click', function () {
    passwordInput.type = 'text';
    showPasswordIcon.style.display = 'none';
    hidePasswordIcon.style.display = 'inline-block';
})

let hidePassword = hidePasswordIcon.addEventListener('click', function () {
    passwordInput.type = 'password';
    hidePasswordIcon.style.display = 'none';
    showPasswordIcon.style.display = 'inline-block';
})


let showConfirmPassword = showPasswordIconConfirm.addEventListener('click', function () {
    confirmPasswordInput.type = 'text';
    showPasswordIconConfirm.style.display = 'none';
    hidePasswordIconConfirm.style.display = 'inline-block';
})

let hideConfirmPassword = hidePasswordIconConfirm.addEventListener('click', function () {
    confirmPasswordInput.type = 'password';
    hidePasswordIconConfirm.style.display = 'none';
    showPasswordIconConfirm.style.display = 'inline-block';
})

let submit = document.querySelector('.btn');

submit.addEventListener('click', (event) => {
    event.preventDefault();
    if (passwordInput.value !== confirmPasswordInput.value) {
        text.innerText = 'Потрібно ввести однакові значення';
    }
    else if (passwordInput.value === '' && confirmPasswordInput.value === '') {
        text.innerText = 'Заповніть значення полів';
    }
    else {
        text.innerText = '';
        alert('You are welcome');
    }
})